======
Assays
======

.. toctree ::
   :maxdepth: 2
   :caption: Contents:

   introduction
   domain-model
   business-scenarios

.. todo :: headlines to much assay
.. todo :: instrument as next domain context of domain model e.g. language, region
.. todo :: qc assay as own quality.
