Business Scenarios
==================

Assay Development Process
-------------------------

.. figure:: drawings/assay-development-process.svg
   :scale: 100 %
   :alt: Assay development process

Assay Production Process
------------------------

.. figure:: drawings/assay-production-process.svg
   :scale: 100 %
   :alt: Assay production process

Assay Examination Process
-------------------------

.. figure:: drawings/assay-examination-process.svg
   :scale: 100 %
   :alt: Assay examination process

Assay Information Flow Process
------------------------------

.. figure:: drawings/assay-info-flow.svg
   :scale: 100 %
   :alt: Assay information flow process

