Introduction
============

This chapter describes the business domain of assay a.k.a IVD test.

It shall elaborate

* on the assay examination (including preparation, extraction and raw
  measurement)
* result computation and representation of the test result.

It shall also elaborate on the different lifecycles of involved subsystems
namely

* the assay device and
* the instrument device

and how they relate to each other.

Instrument and Assay
--------------------

The IVD instrument device (short instrument) is

* Needed to run IVD tests (its purpose)
* Released to the market before or simultaneously with the IVD test
* A medical device (subject to regulatory affairs)
* Independent upon any IVD tests

An IVD test device (short assay)

* Provides a test result about the specific IVD modality it is made for.
  (its purpose)

* Is a medical device (subject to regulatory affairs)
* Requires an instrument to run on.
* Is disposable after one-time use

The instrument has to provide the execution environment for any current
and future IVD tests. The execution environment is composed of

* hardware (actuators, sensors, physical "vessel", etc) where the
  bio-chemical process is run on
* virtual machine (software) where the assay control process and
  result computation runs on.
