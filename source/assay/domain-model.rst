Domain Model
============

Assay Structure Bounded Context
-------------------------------

This context deals with the principle structure of
any assays. without getting specific into details.

Excluded from the (bounded) context is:

* Manufacturing of the assay
* Physical processing of the assay
* Instrument details
* Assay lifetime and instrument lifetime

.. figure:: drawings/assay-domain-model.svg
   :scale: 100 %
   :alt: Assay domain model

Elements of the Assay Domain Model
----------------------------------

.. list-table :: Assay Domain Model
    :widths: 25 75
    :header-rows: 1
    :stub-columns: 1

    * - Element
      - Description
    * - Test Carrier
      - Test carrier can be a strip or a cartridge or any so far not
        not forseen mechanical carrier

        The test carrier holds the reagents and fits physically
        to the instrument.

        It is simplest way it is an Eppendorf tube.
    * - Channel
      - Structure on the test carrier that allows to
        extract and measure one single specific analyte.

        Obviously an Eppendorf tube represents just one channel.
        Many Eppendorf tubes tied together by a rack might represent channels of
        a carrier (the rack).
    * - Lot
      - Set of test carriers produced in one subsequent step.
        I.g. production machine settings are not changed while
        producing, reagents are from the same reagent lot.

        This does certainly not apply to Eppendorf tubes and rather
        apply to more specifically developed carriers.
    * - Assay
      - Synonym for an IVD Test.

        Attributed to clarify the structure of the assay/test.
        e.g. single quantitative assay, single qualitative assay,
        multi quantitative assay. A mixed assay is a multi assay
        containing at least one quantitative and qualitative assay.

        An assay has properties like

        * assay name
        * supported sample types
        * applied sample type
        * allowed regions/countries
        * translation to those countries/languages
    * - Single Assay
      - Examination of one analyte.
        The analyte is examined by running an assay process and
        resulting in digital representation of a raw measure.
        The raw measure is used to compute the assay result.


Important Relations
-------------------

.. list-table :: Relations between assay domain elements
    :widths: 20 20 60
    :header-rows: 1
    :stub-columns: 1

    * - From
      - To
      - Description
    * - Test carrier
      - Channel
      - A test device is composed of the test carrier and is
        made to examine an assay. (1:1 Relation).
        The test carrier can contain multiple channels, as
        well as the assay can be composed of simple assays.
    * - Channel
      - Raw measure
      - It is expected that each channel delivers one measurement
        about the concentration of the (channel sensitive)
        analyte. There might be constellations, where one channel
        can deliver side measures - that are related to the analyte.
    * - Single Assay
      - Raw Measure
      - An assay result can be computed from one measure.

        Some assays are designed to compute the result from multiple
        measures e.g. by averaging or by determining validity
        (e.g. by distribution/outlier analysis, or process control
        measures)
    * - Measurement Calibration
      - Raw Measure
      - Provides a procedure and parameters to correct the raw measurement.
        The procedure and parameters are dependent fro the sample type.


Assay Results
-------------

This section discusses the structure of possible assay results.
The idea is to provide some general understanding which result structures
are common and what alternative result structures are foreseeable.

Result of an qualitative Assay
..............................

Qualitative analysis aims to detect if an analyte is contained in the sample.
Thus, the result is always a binary outcome of either

* **positive** the analyte is contained in the sample
* **negative** the analyte is not contained in the sample.

The quality of a qualitative assay is determined by it's sensitivity,
specificity and also the limit of detection.

.. note ::

  Qualitative analysis is a classification test and in general results
  outcomes that are aligned to categories of arbitrary cardinality.
  However, in terms (bio-)
  chemical assay and the detection of an analyte the outcome is always
  binary with respect to the analyte.

.. todo :: example

Result of an quantitative Assay
...............................

Quantitative analysis aims to detect how much analyte is contained in the
sample.
Thus, the result is always a a discrete or continuous measurement plus a
specific unit of measurement.
the unit of measurement is suitable to address a concentration.
Typical unit of measurements are percentage, mmol/mol, ml/l, mg/l,
particles/mol or similar.

The quality of a quantitative assay is determined by it's accuracy and
precision.
Also the limit of quantification is considered.

.. todo :: example

.. note ::

  * Besides qualitative and quantitative assay no other type of
    assays are forseeable. Both type of assay allow are well established,
    scientifically matured and meed all needs of the market.
  * Multi assays offer an elegant way to provide combinations of
    qualitative and quantitative assay to address the diagnosis of
    specific diseases more efficiently at almost the same cost of
    a single assay.

Model Issues
------------

* Measurement Calibration linked to measurement or rather to single assay?
* Quality Check linked to assay or to lot?
* Are QC Test results a category of it's own?

Physical Assay Processing Bounded Context
-----------------------------------------

Excluded from the (bounded) context is:

* Manufacturing of the assay
* Instrument details
* Assay distribution / lot specific data update

Assay Processing
----------------

.. figure:: drawings/logical-assay-process.svg
   :scale: 100 %
   :alt: Abstract Assay Processing

There are three logical identities involved:

* The operator who triggers the test, applies the sample and does
  some arbitrary (not automated) interactions with the instrument.
* The instrument that might be embodied by a self-contained aparatus
  or might be a set of related/connected aparatus including the
  assay carrier and reagents.
* the assay process, implemented as recipe executed by the operator
  or by the software of the instrument. Part of the software is
  general for all assays (actuator control and sensor reading),
  other parts of that software is specific to the assay.

Prepare Step
............

The prepare step brings the test device
(the test carrier with all its reagents)
into the *ready for sample processing* situation.

The prepare step might contain
some chemical reactions or preparation for reactions that need to
be performed without sample (because it would impact the sample)
and lifts the required reagents from the *storage conservation state*
to the *ready for sample processing* state.

The prepare step requires the control of (bio-) chemical process through
`_actors_and_sensors_`.

Analyte Extraction
..................

After sample is applied a (bio-)chemical process is executed that aims
to bring the analyte into a state where it can be actually be measured.

This (bio-) chemical process is controlled by `_actors_and_sensors_`.

Analyte Measurement
...................

Arbitrary chemicals interact/react with the analyte that cause some
physical measurable effects like photo luminescence. ... The analyte
concentration in the sample is transformed into a digital representation
(image, intensity values, ...) of some physically observable phenomenons.

One-time (spot measurement) require only an appropriate `_measurement_sensor`
to capture the analyte existence or concentration.

In case a time series of measurement is required to derive analyte existence
or concentration optionally some assay processing is required, mostly
without involvement of further reagents.
PCR assays require time series measurement as an example.

.. _sensors_and_actors:

Sensors and Actuators
.....................

The (bio-) chemical assay process is a sequence of steps.

* During a step the external conditions are kept constant.
* Each step results into a new state of the assay that is
  prerequisite for a successful next step.

The purpose of a step can be characterized as

* Bring in a new (bio-) chemical substance (either reagents,
  or specimen (pumping, pipetting)
* Mixing (shaking, pumping, ...)
* Separating (centrifuging, pumping/filtering, ...)
* Control a reaction (maintain a temperature, pressure, ..)

Typical actuators are

* Pumps
  * by servo motors (pwm, dac),
  * step motors (gpio, controller chips)
  * bldc motors (pwm's)
* Valves
  * by magnets (gpio)
* Heater/Pletier elements
  * pwm, adc temp sensor, i2c temp sensor, PID controller

.. _measurement_sensor:

Measurement Sensor
..................

.. TODO :: illumination, camera, spectrometer,


Result Computation
..................

.. todo :: intensity measurements
.. todo :: particle counting measurement
.. todo :: time series analysis (i.e. pcr)
.. todo :: 3 channel validity computation
.. todo :: process control channels
.. todo :: lot calibration application

.. note ::

  * Predefined and pre-qualified methods and procedures are possible
  * New assays might require new methods


Model Issues
------------

* what belongs to the assay?
* what belongs to the instrument?
