Use Case
========

Real Assays
===========

.. list-table :: Overview of assays
    :widths: 15 15 40 30
    :header-rows: 1
    :stub-columns: 1

    * - (Single) Assay
      - Category
      - Purpose
      - Test Result
    * - INR
      - cardio/vascular
      - ex Quick test
      - Quantitative in nl/mol?
    * - D-Dimer
      - cardio/vascular
      - Gerinnungsfaktor
      - Quantitative in nl/mol?
    * - Tnl Assay
      - a
      - b
      - a
