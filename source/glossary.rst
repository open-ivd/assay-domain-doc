Glossary
========

.. glossary ::

    Accuracy and precision
        Accuracy refers to closeness of measurement to a specific value
        while precision refers to the closeness of the measurements to
        each other.

        reference https://en.wikipedia.org/wiki/Accuracy_and_precision

    Analyte
        An analyte, or component, is a substance or chemical constituent that
        is of interest in an analytical procedure. Grammatically speaking,
        experiments always seek to measure properties of analytes—analytes
        themselves can never be measured.

        Reference: https://www.definitions.net/definition/analyte

        Explaining interpretation: The analyte is contained in the the sample

    Assay
        An assay is a quantitative or qualitative test of a substance
        (sample, specimen) to determine its components;
        frequently used to test for the presence or concentration of infectious
        agents or antibodies etc.

        Reference: https://www.definitions.net/definition/assay

        Synonyms: test, examination

    Client
        A client is a person of whom a sample is used for an IVD test.
        Client can be diseased or healthy.

        Synonyms: patient

    Examination
        Set of operations having the object of determining the value of a property.

        The examination of an analyte in a biological sample is commonly referred to
        as a test, assay or analysis.

        Reference: Principles of In Vitro Diagnostic (IVD) Medical Devices Classification
        SG1 Proposed Document (PD/N045R12)

        Synonyms: test, assay

    Instrument
        An instrument is equipment or aparatus intended by a manufacturer
        to be used as IVD Medical Device

        Reference: Principles of In Vitro Diagnostic (IVD) Medical Devices Classification
        SG1 Proposed Document (PD/N045R12)

    IVD Medical Device
        An in vitro diagnostic medical device is a device, whether used alone or in
        combination, intended by the manufacturer for the in-vitro examination of specimens
        derived from the human body solely or principally to provide information
        for diagnostic, monitoring or compatibility purposes.
        This includes reagents, calibrators, control materials, specimen receptacles,
        software, and related instruments or apparatus or other articles.

        Reference: Principles of In Vitro Diagnostic (IVD) Medical Devices Classification
        SG1 Proposed Document (PD/N045R12)

    Limit of Detection (LoD)
        In general, the LOD is taken as the lowest concentration of an analyte
        in a sample that can be detected, but not necessarily quantified,
        under the stated conditions of the test

    Limit of Quantification (LoQ)
        The LOQ is the lowest concentration of an analyte in a sample that can be determined
        with acceptable precision and accuracy under the stated conditions of test.

        see:  Shrivastava A, Gupta VB. Methods for the determination of limit of detection and
        limit of quantitation of the analytical methods. Chron Young Sci [serial online] 2011
        [cited 2020 Mar 3];2:21-5.
        Available from http://www.cysonline.org/text.asp?2011/2/1/21/79345

    Patient
        A person is a person of whom a sample is used for an IVD test.
        A patient is the client of a physician.

        Synonyms: client

    Quality Control (QC)
        Manufacturers of IVD medical devices often include quality control (QC) procedures
        in their instructions for use. These quality control procedures are intended to
        provide users with assurance that the device is performing within specifications,
        and therefore the results are suitable for their intended diagnostic use.
        For some devices, QC procedures can be an essential risk control measure.
        Depending on the design of the device, these quality control procedures can help users
        ensure the quality of results by:

            a. verifying the suitability of analytical systems (sample, reagents, instruments,
               and/or users);

            b. monitoring the precision and trueness of measurement results;
            c. preventing false-negative and false-positive results;
            d. identifying fault conditions that could lead to inaccurate results; and/or
            e. troubleshooting problems that require corrective action.

        Reference: ISO 15198:2004(en)
        https://www.iso.org/obp/ui/#iso:std:iso:15198:ed-1:v1:en

    QC Test
        A qc test is an assay examination that is intended to proof the IVD Test Device and
        Instrument device perform according to the quality requirements. Instead of a sample
        is a prepared reference material used.

    QC Policy
        Is an enforced method to guarantee that IVD test device and instrument device
        continuously meet the quality requirement in the field.

        I typical way to enforce this is to require successful QC test performed on
        defined scheme/schedule.

    Reference material
        Reference material is used as a sample in QC Tests.

    Sample
        A sample is a small part of something intended as representative of the whole

        Reference: https://www.definitions.net/definition/sample

        Synonyms: specimen

    Sensitivity and Specificity
        Sensitivity and specificity are statistical measures of the performance
        of a binary classification test, also known in statistics as
        a classification function, that are widely used in medicine.

        reference https://en.wikipedia.org/wiki/Sensitivity_and_specificity

    Specimen
        A specimen is an example regarded as typical of its class.
        A specimen is a bit of tissue or blood or urine that is taken
        for diagnostic purposes.

        Reference: https://www.definitions.net/definition/specimen

        Synonyms: sample

        Explaining interpretation: Could also be sputum, stool or anything else
        taken from the client.

    Specimen receptacle
        A specimen receptacle is a device, whether vacuum-type or not,
        specifically intended by their manufacturers for the primary containment
        of specimens derived from the human body.

        Reference: Principles of In Vitro Diagnostic (IVD) Medical Devices Classification
        SG1 Proposed Document (PD/N045R12)
