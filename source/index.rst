=======
OpenIVD
=======

Table of Contents
=================

.. toctree ::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   license
   contribution

   assay/index
   workflow/index

   glossary

**OpenIVD** collects and describes business domain knowledge about
**Invitro Diagnostic (IVD) medical products, processes and application**.

**OpenIVD** can be a source of information for

* Product managers and product owners
* Software engineers,
* Requirement engineers,
* V&V and test engineers,

who are involved in engineering of IVD medical products and
who are interested in

* the world of IVD **assays** from the perspective of a computer system without
  providing bio-chemical/physical details of any assay.
* the world of application scenarios and **workflows** in which IVD tests are
  used and how they provide value to the people.


Disclaimer
----------

This documentation is not intended to be a blueprint for any
documents, required for market approval of a medical IVD product.

This documentation does not address regulatory needs at all.
