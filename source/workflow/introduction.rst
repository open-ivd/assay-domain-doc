The World of Assays
===================

This documentation describes business domain of IVD assays.
from the perspective of a software engineer.

The idea is to describe

* the world of IVD assays from information technical perspective without
  providing bio-chemical/physical details of any assay.
* the world of application scenarios and workflows in which IVD tests are
  used and provide value to the society.