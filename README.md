# assay-domain-doc

Domain model for IVD assays. 

* Document is written in sphinx-doc
* And build by .gitlab-ci.yml

The result can be viewed at:
https://open-ivd.gitlab.io/assay-domain-doc
