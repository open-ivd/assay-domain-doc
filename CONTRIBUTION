Contributor license agreement
-----------------------------

By submitting code as an individual you agree
that you contribute to the documentation licensed under
Creative Commons: CC BY 4.0.


How to Contribute to OpenIVD
----------------------------

Propose Modification
....................

You want suggest some modification? E.g.

   * Add more content areas
   * Add more details to a topic
   * Change scope/structure of a topic
   * Change content of a topic
   * Change document structure

View existing issues and potentially submit a new issue at:

https://gitlab.com/open-ivd/assay-domain-doc/-/issues

Fix a Typo or correct Wording
.............................

We are neither perfect, nor is English our mother tongue.
Still, the more accurate and correct this document is is
the more happy we are.

If you spot something and would like to fix it right away do as follow:

* Fork the this repo: https://gitlab.com/open-ivd/assay-domain-doc
* Do modification locally
* Submit a merge requst to the original repo.
  (https://gitlab.com/open-ivd/assay-domain-doc/)
* Somebody will happily take care and merge in.

Writeup significant Content
...........................

* Contact us
* Discuss with us what you have in mind
* Join the team :-)
